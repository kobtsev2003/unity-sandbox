using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Dependencies.NCalc;
using UnityEngine;

public class openScript : MonoBehaviour
{
    public GameObject chest;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.F))
        {
            if (chest.GetComponent<Animator>().GetBool("isOpen"))
            {
                chest.GetComponent<Animator>().SetBool("isOpen", false);
            }
            else
            {
                chest.GetComponent<Animator>().SetBool("isOpen", true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && chest.GetComponent<Animator>().GetBool("isOpen") == true)
        {
            chest.GetComponent<Animator>().SetBool("isOpen", false);
        }
    }
}
