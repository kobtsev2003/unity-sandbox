using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    private float xRotation = 0f;

    public float sensevity = 100f;
    public Transform Player;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * sensevity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensevity * Time.deltaTime;

        xRotation -= mouseY; // �������� �����-���� �� X
        xRotation = Mathf.Clamp(xRotation, 0f, 30f);

        transform.localRotation = Quaternion.Euler(xRotation,0,0);
        Player.Rotate(Vector3.up * mouseX);
    }
}
