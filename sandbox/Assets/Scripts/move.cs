using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    public float speed = 7f;
    public float jump = 300f;
    public bool Ground;
    public Rigidbody rb;
    public Animator animator;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.localPosition += transform.forward * speed * Time.deltaTime;
            animator.SetFloat("speed",speed);
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            animator.SetFloat("speed", 0);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.localPosition += -transform.forward * speed * Time.deltaTime;
            animator.SetFloat("speed", -speed);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            animator.SetFloat("speed", 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.localPosition += -transform.right * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.localPosition += transform.right * speed * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Ground == true)
            {
                rb.AddForce(transform.up * jump);
                animator.SetTrigger("jump");
            }  
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            animator.SetFloat("speed", 0);
            animator.SetTrigger("attack");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            Ground = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            Ground = false;
        }
    }
}
